# React Szkolenie - Todo List - REDUX

<img src="./src/assets/todolist.png" />

## Dostępne komendy:

W folderze projektu dostępne są następujące komendy:

**Instalacja zależności (pierwsze uruchomienie)**
### `npm install`

**Start lokalnego serwera w trybie deweloperskim**
### `npm start`

W przeglądarce: [http://localhost:3000](http://localhost:3000) adres lokalnego serwera.

**Uruchamianie testów**
### `npm test`

**Tworzenie wersji produkcyjnej**
### `npm run build`
gotowy produkt znajduje się w folderze: build


**Dodanie plików konfiguracyjnych do folderu projektu**
#### `npm run eject`
**WAŻNE: użycie operacji EJECT nie pozwala na przywrócenie poprzedniego stanu!**

### Podłączenie providera 

```JavaScript 

import { createStore, applyMiddleware, compose } from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';

import reducers from './Reducers';

const midlewares = {
  thunk
}

////Chrome: ReduxTool
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// const store = createStore(reducers, composeEnhancers( applyMiddleware(...midlewares) ) )

const store = createStore(reducers, applyMiddleware(...midlewares))

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, document.getElementById('root'));

```

### Przykład reducera

```JavaScript
export const ADD_TODO = 'ADD_TODO';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const EDIT_ITEM = 'EDIT_ITEM';
export const CHANGE_STATUS = 'CHANGE_STATUS';

const actions = {
  [ADD_TODO]: (state, action) => {
    const {payload} = action;

    return {
      ...state,
      todoList: [...state.todoList, {...payload}]
    }
  },
  [REMOVE_ITEM]: (state, action) => {
    const {payload: {id}} = action;

    return {
      ...state,
      todoList: state.todoList.filter(v => v.id !== id)
    }
  },
  [EDIT_ITEM]: (state, action) => {
    const {payload: {id, text}} = action;

    const element = [...state.todoList].find(v => v.id === id);
    const elementIndex = [...state.todoList].findIndex(v => v.id === id);
    element.text = text;

    const newTodoList = [...state.todoList];
    newTodoList[elementIndex] = element;

    return {
      ...state,
      todoList: [...newTodoList]
    }
  },
  [CHANGE_STATUS]: (state, action) => {
    const {payload: {id, done}} = action;

    const element = [...state.todoList].find(v => v.id === id)
    const elementIndex = [...state.todoList].findIndex(v => v.id === id);

    element.done = !element.done;

    const newTodoList = [...state.todoList];
    newTodoList[elementIndex] = element;

    return {
      ...state,
      todoList: [...newTodoList]
    }
  }
}


const initialState = {
  todoList: [],
}

export default (state = initialState, action) => {
  const {type} = action;

  if(actions[type]) return actions[type](state, action);
  return state;
}
```

### Akcje

```JavaScript 
import {ADD_TODO, REMOVE_ITEM, EDIT_ITEM, CHANGE_STATUS} from '../Reducers/todoReducers';
export const addTodo = text => {
  return {
    type: ADD_TODO,
    payload: {
      id: Math.random(),
      text,
      done: false
    }
  }
}

export const removeItem = id => {
  return {
    type: REMOVE_ITEM,
    payload: {
      id
    }
  }
}

export const editItem = (id, text) => {
  return {
    type: EDIT_ITEM,
    payload: {
      id,
      text
    }
  }
}

export const changeStatus = (id, done) => {
  return {
    type: CHANGE_STATUS,
    payload: {
      id,
      done
    }
  }
}
```