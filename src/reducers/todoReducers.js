export const ADD_TODO = 'ADD_TODO';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const EDIT_ITEM = 'EDIT_ITEM';
export const CHANGE_STATUS = 'CHANGE_STATUS';

const actions = {
  [ADD_TODO]: (state, action) => {
    const {payload} = action;

    return {
      ...state,
      todoList: [...state.todoList, {...payload}]
    }
  },
  [REMOVE_ITEM]: (state, action) => {
    const {payload: {id}} = action;

    return {
      ...state,
      todoList: state.todoList.filter(v => v.id !== id)
    }
  },
  [EDIT_ITEM]: (state, action) => {
    const {payload: {id, text}} = action;

    const elementIndex = [...state.todoList].findIndex(v => v.id === id);

    const newTodoList = [...state.todoList];
    newTodoList[elementIndex].text = text;

    return {
      ...state,
      todoList: [...newTodoList]
    }
  },
  [CHANGE_STATUS]: (state, action) => {
    const {payload: {id, done}} = action;

    const elementIndex = [...state.todoList].findIndex(v => v.id === id);

    const newTodoList = [...state.todoList];
    newTodoList[elementIndex].done = !newTodoList[elementIndex].done;

    return {
      ...state,
      todoList: [...newTodoList]
    }
  }
}


const initialState = {
  todoList: [],
}

export default (state = initialState, action) => {
  const {type} = action;

  if(actions[type]) return actions[type](state, action);
  return state;
}
