import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import TodoList from "./TodoList";

import {removeItem, editItem, changeStatus} from '../../actions/todoActions';

const mapStateToProps = state => ({
  todoList: state.todo.todoList
})

const mapDispatchToProps = dispatch => ({
  removeItem: bindActionCreators(removeItem, dispatch),
  editItem: bindActionCreators(editItem, dispatch),
  changeStatus: bindActionCreators(changeStatus, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
