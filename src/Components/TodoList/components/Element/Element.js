import React from 'react';

class Element extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      editMode: false
    }
  }

  setEditMode = () => {
    const {text} = this.props;

    this.setState({
      editMode: true
    }, () => {
      this.input.value = text;
      this.input.focus();
    })
  }

  unsetEditMode = () => {
    this.setState({
      editMode: false
    })
  }

  inputChangeHandle = (event) => {
    if((event && event.key === 'Enter') || !event.key){
      this.editValue();
    }
  }

  editValue = () => {
    const {editValue, id} = this.props;
    const {value} = this.input;

    if(value !== '') editValue(id, value);
    this.unsetEditMode();
  }

  getTextOrInput = () => {
    const {number, text} = this.props;
    const {editMode} = this.state;

    if(editMode) return (
      <input
        className="input"
        ref={(input) => this.input = input}
        onBlur={this.inputChangeHandle}
        onKeyDown={this.inputChangeHandle}
      />
    )

    return <span onClick={this.setEditMode}>{number + 1}. {text}</span>
  }

  render(){
    const {id, done, switchStatus, remove} = this.props;

    return(
      <li className={`list-element ${done ? 'complete' : ''} animated fadeIn`}>
        <div className="text">
          <label htmlFor={`checkbox{number}`} className="custom-checkbox"></label>
          <input id={`checkbox{number}`} className="checkbox" type="checkbox" value={done} onClick={() => switchStatus(id)}></input>
          {this.getTextOrInput()}
        </div>
        <div className="options">
          <button className="close" onClick={() => remove(id)}></button>
        </div>
      </li>
    )
  }
}

Element.defaultProps = {
  done: false,
  text: '',
  number: 0,
  remove: () => {},
  switch: () => {}
}

export default Element;
