import React from 'react';
import Element from './components/Element';
import './Todolist.css'

class TodoList extends React.Component {
  getList(){
    const {todoList, removeItem, switchStatus, editItem, changeStatus} = this.props;

    if(todoList.length === 0) return <div>Brak zadań</div>
    return todoList.map((v,i) => (
      <Element
        key={i}
        remove={removeItem}
        switchStatus={changeStatus}
        editValue={editItem}
        text={v.text}
        number={i}
        id={v.id}
        done={v.done}
      />)
    )
  }

  render(){
    return (
      <ul className="todo-list">{this.getList()}</ul>
    )
  }
}

TodoList.defaultProps = {
  todoList: [],
  removeElement: () => {},
  switchStatus: () => {}
}

export default TodoList;
