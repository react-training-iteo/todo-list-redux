import React from 'react';
import './Header.css';

class Header extends React.Component {
  handleAddClick = () => {
    const {addItem} = this.props;
    const {value} = this.input;

    addItem(value);
    this.input.value = '';
  }

  handleEnterClick = ({key, keyCode}) => {
    if(key === 'Enter'){
      const {addItem} = this.props;
      const {value} = this.input;

      addItem(value);
      this.input.value = '';
    }
  }

  render(){
    return (
      <header className="header">
        <h2>Todo List</h2>
        <div className="form-group">
          <input className="input" onKeyDown={this.handleEnterClick} ref={input => {this.input = input} } type="text" />
          <button className="btn" onClick={this.handleAddClick}>Dodaj</button>
        </div>
      </header>
    )
  }
}

Header.defaultProps = {
  addTodo: () => {}
}

export default Header;
