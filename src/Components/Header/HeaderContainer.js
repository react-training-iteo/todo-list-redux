import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Header from "./Header";

import {addTodo} from '../../actions/todoActions';

const mapStateToProps = state => ({
  todoList: state.todo.todoList
})

const mapDispatchToProps = dispatch => ({
  addItem: bindActionCreators(addTodo, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(Header);
