import {ADD_TODO, REMOVE_ITEM, EDIT_ITEM, CHANGE_STATUS} from '../reducers/todoReducers';
export const addTodo = text => {
  return {
    type: ADD_TODO,
    payload: {
      id: Math.random(),
      text,
      done: false
    }
  }
}

export const removeItem = id => {
  return {
    type: REMOVE_ITEM,
    payload: {
      id
    }
  }
}

export const editItem = (id, text) => {
  return {
    type: EDIT_ITEM,
    payload: {
      id,
      text
    }
  }
}

export const changeStatus = (id, done) => {
  return {
    type: CHANGE_STATUS,
    payload: {
      id,
      done
    }
  }
}
