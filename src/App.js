import React, { Component } from 'react';
import Header from './Components/Header';
import TodoList from './Components/TodoList';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="app-container">
        <div className="app">
          <Header />
          <TodoList />
        </div>
      </div>
    );
  }
}

export default App;
